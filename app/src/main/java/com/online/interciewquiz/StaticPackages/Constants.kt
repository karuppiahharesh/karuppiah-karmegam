package com.online.interciewquiz.StaticPackages

import com.online.interciewquiz.PojoPackage.Question

object Constants {

    const val USER_NAME: String = "user_name"
    const val USER_Roll_No: String = "user_roll_no"
    const val TOTAL_QUESTIONS: String = "total_question"
    const val CORRECT_ANSWERS :String= "correct_answers"

    fun getQuestions(): ArrayList<Question>{
        val questionList = ArrayList<Question>()
        val que1 = Question(
            id = 1,
            question = "1. Kotlin is developed by?",
            optionOne = "Google",
            optionTwo = "JetBrains",
            optionThree = "Microsoft",
            optionFour = "Adobe",
            correctAnswer = 2
        )

        questionList.add(que1)

        val que2 = Question(
            id = 2,
            question = "2. Which of following is used to handle null exceptions in Kotlin?",
            optionOne = "Range",
            optionTwo = "Sealed Class",
            optionThree = "Elvis Operator",
            optionFour = "Lambda function",
            correctAnswer = 3
        )

        questionList.add(que2)

        val que3 = Question(
            id = 3,
            question = "3. Which file extension is used to save Kotlin files.",
            optionOne = ".java",
            optionTwo = ".kot",
            optionThree = ".kt or .kts",
            optionFour = ".andriod",
            correctAnswer = 3
        )
        questionList.add(que3)




        val que4 = Question(
            id = 4,
            question = "4. All classes in Kotlin classes are by default?",
            optionOne = "public",
            optionTwo = "final",
            optionThree = "sealed",
            optionFour = "abstract",
            correctAnswer = 2
        )
        questionList.add(que4)

        val que5 = Question(
            id = 5,
            question = "5. Which of follwing targets currently not supported by Kotlin?",
            optionOne = "LLVM",
            optionTwo = ".NET CLR",
            optionThree = "Javascript",
            optionFour = "Java",
            correctAnswer = 2
        )
        questionList.add(que5)



        val que6 = Question(
            id = 6,
            question = "6. Kotlin is developed by?",
            optionOne = "Google",
            optionTwo = "JetBrains",
            optionThree = "Microsoft",
            optionFour = "Adobe",
            correctAnswer = 2
        )

        questionList.add(que6)

        val que7 = Question(
            id = 7,
            question = "7. Which of following is used to handle null exceptions in Kotlin?",
            optionOne = "Range",
            optionTwo = "Sealed Class",
            optionThree = "Elvis Operator",
            optionFour = "Lambda function",
            correctAnswer = 3
        )

        questionList.add(que7)

        val que8 = Question(
            id = 8,
            question = "8. Which file extension is used to save Kotlin files.",
            optionOne = ".java",
            optionTwo = ".kot",
            optionThree = ".kt or .kts",
            optionFour = ".andriod",
            correctAnswer = 3
        )
        questionList.add(que8)




        val que9 = Question(
            id = 9,
            question = "9. All classes in Kotlin classes are by default?",
            optionOne = "public",
            optionTwo = "final",
            optionThree = "sealed",
            optionFour = "abstract",
            correctAnswer = 2
        )
        questionList.add(que9)

        val que10 = Question(
            id = 10,
            question = "10. Which of follwing targets currently not supported by Kotlin?",
            optionOne = "LLVM",
            optionTwo = ".NET CLR",
            optionThree = "Javascript",
            optionFour = "Java",
            correctAnswer = 2
        )
        questionList.add(que10)

        return questionList
    }
}