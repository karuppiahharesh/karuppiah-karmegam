package com.online.interciewquiz.ActivityPackage

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.online.interciewquiz.StaticPackages.Constants
import com.online.interciewquiz.R
import kotlinx.android.synthetic.main.activity_result.*

class Result : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)
        val username = intent.getStringExtra(Constants.USER_NAME)
        val userrollno = intent.getStringExtra(Constants.USER_Roll_No)
        tv_name.text = username
        tv_rollno.text ="("+ userrollno +")"

        val totalQuestions = intent.getIntExtra(Constants.TOTAL_QUESTIONS, 0)
        val correctAnswer = intent.getIntExtra(Constants.CORRECT_ANSWERS, 0)
if(correctAnswer >= 5){
    tv_result.text = ("Result : Pass")
    congratulations.visibility = View.VISIBLE
}else{
    tv_result.text = ("Result : Fail")
    congratulations.visibility = View.GONE
}


        tv_score.text = "Your score is $correctAnswer out of $totalQuestions"

        val total: Double = correctAnswer.toString().toDouble()

        val res = (total / 10f) * 100

        totalpers.text = res.toString()+"%"

        btn_finish.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }
}