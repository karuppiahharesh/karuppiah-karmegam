package com.online.interciewquiz.ActivityPackage

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import android.widget.Toast
import com.online.interciewquiz.StaticPackages.Constants
import com.online.interciewquiz.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN)
       // window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN

        btn_start.setOnClickListener {
            if (et_name.text.toString().isEmpty() || et_rollno.text.toString().isEmpty() ){
                Toast.makeText(this, "Please enter your name and roll number...", Toast.LENGTH_SHORT).show()
            } else
            {
                intent = Intent(this, QuizQuestionsActivity::class.java)
                intent.putExtra(Constants.USER_NAME, et_name.text.toString())
                intent.putExtra(Constants.USER_Roll_No, et_rollno.text.toString())
                startActivity(intent)
                finish()
            }
        }
    }
}